# err-question

Errbot plugin that looks for questions.

## Install

`!repos install https://gitlab.com/jaquerespeis/err-question`

## Configuration

```
!plugin config Question {'source_group_id': 'change me', 'question_group_id': 'change me'}
```

## Usage

When a message sent to the `source_group` is identified as a question, it is
forwarded to the `question_group`.
