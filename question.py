import errbot


class Question(errbot.BotPlugin):

    def get_configuration_template(self):
        return {
            'source_group_id': 'change me',
            'question_group_id': 'change me',
        }

    def callback_message(self, message):
        if (self.is_from_source_group(message)
                and self.is_question(message.body)):
            self.send(
                self.build_identifier(self.config['question_group_id']),
                '{} pregunta: \n{}'.format(
                    message.frm.username or message.frm.first_name,
                    message.body
                ))

    def is_from_source_group(self, message):
        return (message.is_group and
            message.frm.room.id == self.config['source_group_id'])

    def is_question(self, message):
        return '?' in message
